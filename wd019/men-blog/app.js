const express = require("express") //package na express
const app = express(); // express is a function that was installed in app variable.
const PORT = 4000; //declare a default port 
const mongoose = require("mongoose")


mongoose.connect("mongodb://localhost/b49_blog", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})


const db = mongoose.connection

db.once('open', () => console.log("You are now connected to MongoDB"))



app.use(express.json())

app.use("/posts", require("./routes/posts"))
app.use("/users", require("./routes/users"))
app.listen(PORT, () => console.log("Server is now running in port: " + PORT))


