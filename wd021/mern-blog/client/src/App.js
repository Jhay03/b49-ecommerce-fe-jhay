import React, {Fragment, useEffect, useState} from 'react';
import Navbar from './components/layouts/Navbar';
import {
  BrowserRouter as Router,
  Switch,
  Route
}
  from "react-router-dom";
import Login from './components/forms/Login';
import Register from './components/forms/Register';
import Posts from './components/Posts';
import AddPost from './components/forms/AddPost';
import Auth from './components/Auth'
import Post from './components/Post';
import Profile from './components/Profile';


function App() {
  const [posts, setPosts] = useState([])
  const [user, setUser] = useState({})
  const [token, setToken] = useState("")
  useEffect(() => {
    fetch("http://localhost:4000/posts")
      .then(res => res.json())
      .then(data => {
      //console.log(data)
        setPosts(data)
      })
      setUser(JSON.parse(localStorage.getItem("user")))
      setToken(localStorage.getItem("token"))
  }, [posts])

  
  const logoutHandler = () => {
    localStorage.clear()
    setUser({})
    setToken("")
  }
  

  
  return (
    <Router>
      <Fragment>
        <Navbar user={user} token={token} logoutHandler={logoutHandler} />
          <Switch>
              <Route path="/login">
                  <Login/>
              </Route>
              <Route path="/register">
                  <Register/>
              </Route>
              <Route path="/add-post">
            {user && token ? <AddPost token={token}/> : <Auth/>}
          </Route>
          <Route path="/posts/:id">
            <Post
              user={user}
              token={token}
              
            />
          </Route>
          <Route exact path="/">
            <Posts posts={posts}/>
          </Route>      
          <Route path="/profile">
            <Profile posts={posts} user={user} token={token} />
          </Route>
          </Switch>
      </Fragment>
    </Router>  
  );
}

export default App;
