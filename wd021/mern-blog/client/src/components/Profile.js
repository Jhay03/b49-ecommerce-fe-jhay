import React from 'react';
import { Link } from 'react-router-dom';

const Profile = ({ posts, user, token }) => {
    let filteredPost = posts.map(post => (
        <div className="container" key={post._id}>
            {user && post.author === user.username && token ?
                <div className="row">
                    <div className="col col-lg-4">
                        <div className="card mb-3" key={post.id}>
                            <div className="card-body">
                                <h4 className="card-title">{post.title}</h4>
                                <p className="card-text">{post.description}</p>
                                <small>{post.author}</small>
                                <br />
                                <Link to={`/posts/${post._id}`}> View Post Details</Link>
                            </div>
                        </div>
                    </div>
                </div> : null }
        </div>
    ))
   
    
    return (
        
        <div className="container col-sm-6 mt-2">
            <h2>Your Post(s)</h2>
            <hr />
           
                { filteredPost }
                
        
                
        </div>
           
    )
}

export default Profile;