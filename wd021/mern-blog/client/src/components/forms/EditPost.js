import React, {useState} from 'react';

const EditPost = ({ setShowEdit, post, token }) => {
    const [newEdit, setNewEdit] = useState({
        id: post._id,
        title: post.title,
        description: post.description,
        author: post.author
    })
  
    const onChangeHandler = (e) => {
        setNewEdit({
            ...newEdit,
            [e.target.name]: e.target.value
        })
        
    }
    
    const onSubmitHandler = () => {
        //console.log(newEdit)

        fetch(`http://localhost:4000/posts/${post._id}`, {
      method: "PUT",
      body: JSON.stringify(newEdit),
      headers: {
          "Content-Type": "application/json",
          "x-auth-token": token
      }
    })
      .then(res => res.json())
            .then(data => {
        setShowEdit(false)
    })
  }
    return (
        <div className="container">
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input type="text"
                    name="title"
                    id="title"
                    className="form-control"
                    value={newEdit.title}
                    onChange={onChangeHandler}
                />
            </div>
            <div className="form-group">
                <label htmlFor="description">Description</label>
                <input type="text"
                    name="description"
                    id="description"
                    className="form-control"
                    value={newEdit.description}
                    onChange={onChangeHandler}
                />
            </div>
            <button className="btn btn-outline-success mb-2 mr-2" onClick={()=>onSubmitHandler()}>Update</button>
            <button className="btn btn-outline-primary mb-2" onClick={() => setShowEdit(false)}>Cancel</button>

        </div>
    )
}

export default EditPost;