import React, {Fragment} from "react"
import { Link } from "react-router-dom"
const Navbar = ({ user, token, logoutHandler}) => {
	let guestLinks;
	let authLinks;
	if (!user && !token) {
		guestLinks = (
			<Fragment>
				<li className="nav-item">
						<Link to="/register" className="nav-link">Register</Link>
					</li>
					<li className="nav-item">
						<Link to="/login" className="nav-link">Login</Link>
				</li>
			</Fragment>
		)
	}
	if (token && user) {
		authLinks = (
			<Fragment>
				<li className="nav-item">
					<Link to="/login"
						className="nav-link"
						onClick={logoutHandler}>
						Logout</Link>
				</li>
				<Link to="/profile"
					className="nav-link"
				>{user.fullname}
				</Link>
				<li className="nav-item">
						<Link to="/add-post" className="nav-link">Add Post</Link>
				</li>
			</Fragment>
		)
	}
	return (
		<nav className="navbar navbar-dark bg-dark navbar-expand-sm">
			<Link to="/" className="navbar-brand">B49-Blog</Link>
 		    <button
				className="navbar-toggler"
				type="button"
				data-toggle="collapse"
				data-target="#navbarNav">
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="navbarNav">
				<ul className="navbar-nav ml-auto">
					<li className="nav-item">
						<Link to="/" className="nav-link">All Posts</Link>
					</li>
					{authLinks}
					{guestLinks}
				</ul>
			</div>
		</nav>
	)
}
export default Navbar