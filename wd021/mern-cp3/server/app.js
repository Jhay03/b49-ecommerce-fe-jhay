const express = require("express")
const app = express()
const PORT = process.env.PORT ||  4000;
const cors = require("cors")
const mongoose = require("mongoose")

mongoose.connect("mongodb://localhost/b49-capstone_3", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

const db = mongoose.connection
db.once('open', () => console.log("You are now connected on MongoDB"))

app.use(cors())
app.use(express.json())

//will define the routing
app.use("/users", require("./routes/users"))

app.listen(PORT, () => console.log(`You are now connected to PORT : ${PORT}`))
