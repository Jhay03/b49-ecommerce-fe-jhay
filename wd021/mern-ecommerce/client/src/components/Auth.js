import React from 'react';
import { Link } from 'react-router-dom';

const Auth = () => {
    return (
        <div className="container col-sm-6 mx-auto">
        <h3>Unauthorized! You must be logged in as Admin.</h3> 
         <h2>
             <Link to="/login"> Login</Link>
         </h2>
     </div>
    )
}

export default Auth