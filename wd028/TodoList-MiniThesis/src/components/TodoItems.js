import React, {useState} from 'react';
import EditTodo from './forms/EditTodo';



const TodoItems = (
    {
        todo: { userId, id, title, isCompleted },
        editTodo,
        deleteTodo
    }
) => {
    const [editing, setEditing] = useState(false)
/*
    const getStyle = () => {
        return {
            backgroundColor: 'gray',
            padding: '1px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none'
        }
    }*/


    const style = {
        backgroundColor: "red",
        color: "white",
        padding: "10px",
        textAlign: "center",
        marginBottom: "10px",
        borderBottom: '1px #ccc dotted',
    }
    return (
        <div style={style}>
            {editing ?
                <EditTodo
                    setEditing={setEditing}
                    title={title}
                    id={id}
                    editTodo={editTodo}
                />
                :
                <div>
                    <small>{userId}</small>
                    <small>{id}</small>
                    <p>{title}</p>
                    <input type="checkbox"></input>
                    <br/>
                    {isCompleted}
                    <button onClick={() => setEditing(true)}> Edit </button>
                    <button onClick={() => deleteTodo(id)}> Delete</button>
                </div>
                
                }
          
        </div>
    )
}

export default TodoItems;
