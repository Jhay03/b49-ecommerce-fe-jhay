import React, {useState } from 'react';


   
const AddTodo = ({ addTodo }) => {
    const [formData, setFormData] = useState({
        title: ""
       
    })
    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    return (
        <form onSubmit={(e) => {
            e.preventDefault()
            addTodo(formData)
        }}>
            <label htmlFor="title">Title</label>
            {JSON.stringify(formData)}
            <input
                name="title"
                type="text"
                value={formData.title}
                onChange={onChangeHandler}
            />
            <br/>
            <label> Is completed: </label>
            <input type="checkbox" name="status" />
            <br/>
            <button> Submit</button>
        </form>
    )
}


export default AddTodo;