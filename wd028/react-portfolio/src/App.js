import React, {Fragment} from 'react';
import Navbar from './Navbar';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Contact from './Contact';
import Projects from './Projects';
import Home from './Home';
import About from './About';
function App() {
  return (
    <Router>
      <Fragment>
        <Navbar />
        <Switch>

          <Route path="/contact">
            <Contact />
          </Route>

          <Route path="/projects">
            <Projects />
          </Route>
          
          <Route path="/">
            <Home />
          </Route>

          <Route path="/about">
            <About />
          </Route>
          
        </Switch>
      </Fragment>
    </Router>

  );
}

export default App;
